/**
 *  Copyright (c) 2015, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 */

// Model types
class Salon {}
class Master {}

// Mock data
var viewer = new Salon();
viewer.id = '1';
viewer.name = 'Anonymous';

var masters = ['Test1', 'Test12', 'Test3'].map((name, i) => {
	var master = new Master();
	master.name = name;
	master.id = `${i}`;
	return master;
});

module.exports = {
	// Export methods that your schema can use to interact with your database
	getSalon: (id) => id === viewer.id ? viewer : null,
	getViewer: () => viewer,
	getMaster: (id) => masters.find(m => m.id === id),
	getMasters: () => masters,
	Salon,
	Master,
};
