/**
 *  Copyright (c) 2015, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 */

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
} from 'graphql-relay';

import {
  // Import methods that your schema can use to interact with your database
  Salon,
  Master,
  getSalon,
  getViewer,
  getMaster,
  getMasters,
} from './database';

/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
var {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    var {type, id} = fromGlobalId(globalId);
    if (type === 'Salon') {
      return getSalon(id);
    } else if (type === 'Master') {
      return getMaster(id);
    } else {
      return null;
    }
  },
  (obj) => {
    if (obj instanceof Salon) {
      return SalonType;
    } else if (obj instanceof Master)  {
      return MasterType;
    } else {
      return null;
    }
  }
);

/**
 * Define your own types here
 */

var SalonType = new GraphQLObjectType({
  name: 'Salon',
  description: 'A salon who uses our app',
  fields: () => ({
    id: globalIdField('Salon'),
    masters: {
      type: masterConnection,
      description: 'Salon\'s masters collection',
      args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getMasters(), args),
    },
  }),
  interfaces: [nodeInterface],
});

var MasterType = new GraphQLObjectType({
  name: 'Master',
  description: 'Master who works in our Salon',
  fields: () => ({
    id: globalIdField('Master'),
    name: {
      type: GraphQLString,
      description: 'The name of the Master',
    },
  }),
  interfaces: [nodeInterface],
});

/**
 * Define your own connection types here
 */
var {connectionType: masterConnection} =
  connectionDefinitions({name: 'Master', nodeType: MasterType});

/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */
var queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    // Add your own root fields here
    viewer: {
      type: SalonType,
      resolve: () => getViewer(),
    },
  }),
});

/**
 * This is the type that will be the root of our mutations,
 * and the entry point into performing writes in our schema.
 */
var mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    // Add your own mutations here
  })
});

/**
 * Finally, we construct our schema (whose starting query type is the query
 * type we defined above) and export it.
 */
export var Schema = new GraphQLSchema({
  query: queryType,
  // Uncomment the following after adding some mutation fields:
  // mutation: mutationType
});
